<?php

ini_set('display_errors', 1);
error_reporting( E_ALL ^ E_NOTICE );
require("../App/App.php");

// Version
define('VERSION', '1.0');

$dirName = str_replace( "Public", "App", __DIR__);

define( 'APP_PATH', $dirName );
define( 'URL', 'http://rockstack.co:8080' );


// Configuration
if (file_exists('../App/Conf.php')) {
	require_once('../App/Conf.php');
}

require_once('../App/Conf.php');

// Autoload
require_once('../App/Core/Common/Autoload.php');

if( empty( $conf ) ){

	throw new Exception("No existe la configuración", 1);
}

$app = new Core\App( $conf );
$app->run();

