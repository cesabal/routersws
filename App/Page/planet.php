<?php

use Core\Controller\Page;
use Core\Ws\WsManager;

/**
 * La clase se encarga de obtener usduarios desde el api https://randomuser.me
 * obteniendo el de mayor edad a la ingresada
 */

class planet extends Page{
	

	/**
	 * Contiene el manager ws
	 * @var Ws\WsManager
	 */
	var $manager;

	/**
	 * El index de la clase de ejecuta al inicarla, el metodo va a obtener
	 * una lista de 10 oersonas desde el api https://randomuser.me
	 * y los ordenará por nombre
	 */
	public function index()
	{
		
		if( isset( $_POST['reqtxt'] ) &&  $_POST['reqtxt'] != '' )
		{
			// Url del api de usuarios
			// Se hace la petición al primer planeta, y se recorren las personas
			$urlApiSw = "https://swapi.co/api/planets/";

			// Se crea la instancia del wsmanager
			$this->manager = new WsManager();

			$next = $urlApiSw;
			$control = 10;
			$cont = 0;
			
			$planet = array();

			while ( $next != "" ) {
				
				$cont++;
				if( $cont >= $control ){
					print 'se cancela = ' . $cont;
					break;
				}

				$data = $this->getData( $next );

				if( $data )
				{
					// Se recorre el arreglo de planetas de esta pagina
					foreach( $data->results as $currentPlanet ){
						$maxpopulation = 
						$terrains = explode(",", $currentPlanet->terrain);
						if( in_array( $_POST['reqtxt'], $terrains) )
						{
							$planet[] = array( 'population' => $currentPlanet->population, "planet" => $currentPlanet );
						}
					}

					$populations = array_column ( $planet ,"population");

					// Si tiene un link de siguiente seguira con la siguiente pagina
					$next = $data->next;
				}
				else
				{
					$next = "";
				}

			}

			// El de maxima poblacion
			$maxPopulation = max($populations);
			foreach( $planet as $currentPlanet ){
				
				if( $currentPlanet['population'] == $maxPopulation ){
					$planet = $currentPlanet;
				}
			}

			$this->data['terrain'] = $_POST['reqtxt'];
			$this->data['planets'] = $planet;
		}
		else{
			$this->data['Nousers'] = "Ingresa una edad";
		}
		$this->setTemplateFile('test/planet.php');
		
	}



	/**
	 * [Obtiene una lista de usuarios y los agrega a la variable de clase $this->data
	 * @param  [String] $urlApiSw Url del api a obtener los usuarios
	 */
	private function getData( $urlApiSw ){

		$response = array();
		$err = array();
		$header = array();

		$this->manager->sendCurl( $urlApiSw, $response, $err, $header );

		if( $response ){
			$response = json_decode($response);
			return $response;
		}

		return false;

	}


}