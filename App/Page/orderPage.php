<?php

use Core\Controller\Page;
use Core\Ws\WsManager;

/**
 * La clase se encarga de obtener una lista de usuarios desde el api https://randomuser.me
 * y ordenarla por nombres
 */
class OrderPage extends Page{
	
	/**
	 * Variable que contiene los usuarios
	 * @var array
	 */
	var $users = array();

	/**
	 * Contiene el manager ws
	 * @var Ws\WsManager
	 */
	var $manager;

	/**
	 * El index de la clase de ejecuta al inicarla, el metodo va a obtener
	 * una lista de 10 oersonas desde el api https://randomuser.me
	 * y los ordenará por nombre
	 */
	public function index()
	{
		// Url del api de usuarios
		$urlApiUsers = "https://randomuser.me/api/?results=10";

		// Se crea la instancia del wsmanager
		$this->manager = new WsManager();

		// $this->getRandomUsers( $urlApiUsers, 10 );// [DEPRECATED] el api retorna la cantidad solicitada
		// Se obtienen los resultados de los usuarios, 10 en este caso
		$this->getRandomUsersByresults( $urlApiUsers );
		
		// Ordenado por nombres, ya que el arreglo llega organizado copn keys  [nombre][apellido]
		ksort( $this->users );

		// Inicializo la variable que se pasa a la vista
		$userToprint = array();

		// Se recorre el array para ordenar pr apellido y armar la variable de salida
		foreach ($this->users as $keyName => $userBylast ) {

			// garantiza que los apelllidos tambien estpen ordenados
			ksort($userBylast);

			foreach( $userBylast as $keyLastName => $user ) {

				$userToprint[] = array(
					"name" => $keyName,
					"last" => $keyLastName,
					"email" => $user->email
				);
			}
		}

		// Se envia a la vista
		$this->data['users'] = $userToprint;
		$this->setTemplateFile('test/order.php');
		
	}

	/**
	 * [DEECATED]
	 * @param  [type] $urlApiUsers [description]
	 * @param  [type] $numUsers    [description]
	 * @return [type]              [description]
	 */
	private function getRandomUsers( $urlApiUsers, $numUsers ){

		$response = array();
		$err = array();
		$header = array();


		$numUsers = (Int)$numUsers;
		
		for( $i=0; $i <= $numUsers; $i++ ){

			$this->manager->sendCurl( $urlApiUsers, $response, $err, $header );

			if( $response ){
				$response = json_decode($response);
				$this->addUserByName( $response->results[0] );
			}

		}
	}

	/**
	 * [Obtiene una lista de usuarios y los agrega a la variable de clase $this->users
	 * @param  [String] $urlApiUsers Url del api a obtener los usuarios
	 */
	private function getRandomUsersByresults( $urlApiUsers ){
		$response = array();
		$err = array();
		$header = array();

		$this->manager->sendCurl( $urlApiUsers, $response, $err, $header );

		if( $response ){
			$response = json_decode($response);
			
			foreach( $response->results as $user ) {
				$this->users[ $user->name->first ][ $user->name->last ] = $user;
			}
			
			// $this->addUserByName( $response->results[0] );
		}

	}

	/**
	 * Agrega usuarios desde el webservice
	 * @param [type] $user [description]
	 */
	private function addUserByName($user){
		// echo '<pre>USER'; print_r(); echo '</pre>';

		$this->users[ $user->name->first ][ $user->name->last ] = $user;
	}

}