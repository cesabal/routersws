<?php

use Core\Controller\Page;
use Core\Ws\WsManager;

/**
 * La clase se encarga de obtener usduarios desde el api https://randomuser.me
 * obteniendo el de mayor edad a la ingresada
 */

class FindPage extends Page{
	

	/**
	 * Variable que contiene los usuarios
	 * @var array
	 */
	var $users = array();

	/**
	 * Contiene el manager ws
	 * @var Ws\WsManager
	 */
	var $manager;

	/**
	 * El index de la clase de ejecuta al inicarla, el metodo va a obtener
	 * una lista de 10 oersonas desde el api https://randomuser.me
	 * y los ordenará por nombre
	 */
	public function index()
	{
		
		if( isset( $_POST['edadtxt'] ) &&  $_POST['edadtxt'] != '' )
		{
			$edadToCompare = (Int)$_POST['edadtxt'];

			// Url del api de usuarios
			// $urlApiUsers = "https://randomuser.me/api/?results=100";
			$urlApiUsers = "https://randomuser.me/api/";

			// Se crea la instancia del wsmanager
			$this->manager = new WsManager();

			// $this->getRandomUsers( $urlApiUsers, 10 );// [DEPRECATED] el api retorna la cantidad solicitada
			// Se obtienen los resultados de los usuarios, 10 en este caso
			$control = 20;
			$age = 0;
			$cont = 0;
			
			while ( $age < $edadToCompare ) {
				
				$cont++;
				if( $cont >= $control ){
					print 'se cancela = ' . $cont;
					break;
				}

				$this->getRandomUser( $urlApiUsers );
				$age = $this->users->dob->age;

			}

			// Se envia a la vista
			$this->data['edadIngresada'] = $edadToCompare;
			$this->data['edad'] = $age;
			$this->data['users'] = $this->users;
		}
		else{
			$this->data['Nousers'] = "Ingresa una edad";
		}
		$this->setTemplateFile('test/find.php');
		
	}



	/**
	 * [Obtiene una lista de usuarios y los agrega a la variable de clase $this->users
	 * @param  [String] $urlApiUsers Url del api a obtener los usuarios
	 */
	private function getRandomUser( $urlApiUsers ){
		$response = array();
		$err = array();
		$header = array();

		$this->manager->sendCurl( $urlApiUsers, $response, $err, $header );

		if( $response ){
			$response = json_decode($response);
			
			foreach( $response->results as $user ) {
				$this->users = $user;
			}
			
		}

	}


}