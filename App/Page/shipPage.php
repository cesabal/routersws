<?php

use Core\Controller\Page;
use Core\Ws\WsManager;

/**
 * La clase se encarga de obtener usduarios desde el api https://randomuser.me
 * obteniendo el de mayor edad a la ingresada
 */

class ShipPage extends Page{
	

	/**
	 * Contiene el manager ws
	 * @var Ws\WsManager
	 */
	var $manager;

	/**
	 * El index de la clase de ejecuta al inicarla, el metodo va a obtener
	 * una lista de 10 oersonas desde el api https://randomuser.me
	 * y los ordenará por nombre
	 */
	public function index()
	{
		
		if( isset( $_POST['reqtxt'] ) &&  $_POST['reqtxt'] != '' )
		{
			$peoples = (Int)$_POST['reqtxt'];

			// Url del api de usuarios
			// Se hace la petición al primer planeta, y se recorren las personas
			$urlApiSw = "https://swapi.co/api/starships/";

			// Se crea la instancia del wsmanager
			$this->manager = new WsManager();

			$next = $urlApiSw;
			$control = 10;
			$cont = 0;
			
			$ship = array();

			while ( $next != "" ) {
				
				$cont++;
				if( $cont >= $control ){
					print 'se cancela = ' . $cont;
					break;
				}

				$data = $this->getData( $next );

				if( $data )
				{
					// Se recorre el arreglo de naves de esta pagina
					foreach( $data->results as $starship ){
						
						$actualTime = time();
						$capacity = strtotime("+" . $starship->consumables );
						$capacityWeek = strtotime("+1 week" ) - time();
						$currentCapacity = (Int)$capacity - (Int)$actualTime;

						// Condicion de si tiene la capacidad de pasajeros
						if( $starship->passengers >= $_POST['reqtxt'] )
						{
							// Condicion si puede ir mas de una semana
							if( $currentCapacity > $capacityWeek ){

								// Condicion de si áparece en los films 4, 5, 6
								foreach( $starship->films as $film ){

									// Obtengo el numero del film
									preg_match_all("/\/(\d)\//", $film, $matches);
									$currentFilm = $matches[1][0];

									// si cumple condiciones lo tomo
									if( $currentFilm == 4 || $currentFilm == 5 || $currentFilm == 6 ){
										$ship[] = $starship;
									}
								}

							}
						}
					}

					// Si tiene un link de siguiente seguira con la siguiente pagina
					$next = $data->next;
				}
				else
				{
					$next = "";
				}

			}


			$speeds = array();

			foreach( $ship as $currentShip ){
				$speeds[] = (Int)$currentShip->max_atmosphering_speed;
			}

			foreach( $ship as $currentShip ){
				if( max($speeds) == $currentShip->max_atmosphering_speed ){
					$ship = $currentShip;
				}

			}

			$this->data['passengers'] = $_POST['reqtxt'];
			$this->data['ships'] = $ship;
		}
		else{
			$this->data['Nousers'] = "Ingresa una edad";
		}
		$this->setTemplateFile('test/ship.php');
		
	}



	/**
	 * [Obtiene una lista de usuarios y los agrega a la variable de clase $this->data
	 * @param  [String] $urlApiSw Url del api a obtener los usuarios
	 */
	private function getData( $urlApiSw ){

		$response = array();
		$err = array();
		$header = array();

		$this->manager->sendCurl( $urlApiSw, $response, $err, $header );

		if( $response ){
			$response = json_decode($response);
			return $response;
		}

		return false;

	}


}