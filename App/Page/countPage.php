<?php

use Core\Controller\Page;
use Core\Ws\WsManager;

/**
 * La clase se encarga de obtener una lista de usuarios desde el api https://randomuser.me
 * y contar la letra mas usada en los nombres
 */
class countPage extends Page{
	

	/**
	 * Variable que contiene los usuarios
	 * @var array
	 */
	var $users = array();

	/**
	 * Contiene el manager ws
	 * @var Ws\WsManager
	 */
	var $manager;

	/**
	 * El index de la clase de ejecuta al inicarla, el metodo va a obtener
	 * una lista de 10 oersonas desde el api https://randomuser.me
	 * y los ordenará por nombre
	 */
	public function index()
	{
		// Url del api de usuarios
		$urlApiUsers = "https://randomuser.me/api/?results=5";

		// Se crea la instancia del wsmanager
		$this->manager = new WsManager();

		// $this->getRandomUsers( $urlApiUsers, 10 );// [DEPRECATED] el api retorna la cantidad solicitada
		// Se obtienen los resultados de los usuarios, 10 en este caso
		$this->getRandomUsersByresults( $urlApiUsers );
		
		// Almacenará un srting con las letras de los nombres
		$fullStringNames = "";
		
		// Se recorre el array para armar un string con todos los nombres
		foreach ($this->users as $keyName => $userBylast ) {
			$fullStringNames .= $keyName;
		}

		// Consigo un arerglo con todas las letras que se usaron en los nombres
		$arrayLetters = str_split( $fullStringNames );
		$frecuencies = array_count_values( $arrayLetters );

		// Obtiene la key de la maxima frecuencia que es la letra
		$maxLetter = array_keys($frecuencies, max($frecuencies));

		// La frecuencia de la letra mas usada
		$frecuency = max($frecuencies);

		// Se envia a la vista
		$this->data['users'] = $this->users;
		$this->data['letter'] = $maxLetter[0];
		$this->data['frec'] = $frecuency;
		$this->setTemplateFile('test/count.php');
		
	}

	/**
	 * [Obtiene una lista de usuarios y los agrega a la variable de clase $this->users
	 * @param  [String] $urlApiUsers Url del api a obtener los usuarios
	 */
	private function getRandomUsersByresults( $urlApiUsers ){
		$response = array();
		$err = array();
		$header = array();

		$this->manager->sendCurl( $urlApiUsers, $response, $err, $header );

		if( $response ){
			$response = json_decode($response);
			
			foreach( $response->results as $user ) {
				$this->users[ $user->name->first ][ $user->name->last ] = $user;
			}
			
			// $this->addUserByName( $response->results[0] );
		}

	}


}