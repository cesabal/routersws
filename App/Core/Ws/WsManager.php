<?php

namespace Core\Ws;

class WsManager{
	

	/**
	 * Constructor de la clase, se encarga de realizar inicializaciones de variables
	 * desde la configuración de spotify
	 */
	public function __construct(){

		
		
	}

	public function sendCurl( $url, &$response, &$err, &$header )
	{
		
		 $options = array(
	        CURLOPT_RETURNTRANSFER => true,     // return web page
	        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
	        CURLOPT_ENCODING       => "",       // handle all encodings
	        CURLOPT_TIMEOUT        => 120,      // timeout on response
	        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
	        CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
	        CURLOPT_HTTPHEADER     => array(
				'Accept: application/json',
				'Content-Type: application/json',
				// 'Authorization: Bearer ' .  $accessToken->access_token
			)
	    );

	    $ch      = curl_init( $url );

	    curl_setopt_array( $ch, $options );

	    $content = curl_exec( $ch );
	    $err     = curl_errno( $ch );
	    $errmsg  = curl_error( $ch );
	    $header  = curl_getinfo( $ch );

	    curl_close( $ch );

	    $response = $content;

	}
}