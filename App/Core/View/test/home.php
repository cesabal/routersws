<h1>
	Home
</h1>

<div>
	<h3>Preuba técnica: <?php echo $nombre ?></h3>

	<p>Realicé los cinco puntos de la prueba, en 5 pantallas diferentes, quise hacer un modelo rápido mvc, con algunos enrutadores, y algunos detalles mas, mi intención, que se puedan ver mis habilidades y mis conocimientos en la programación orientada a objetos, en el entendimiento del modelo mvc, en el dominio del php, en los detalles de presentación, un gusto resolver la prueba, muchas gracias!</p>

	<p>se detallan 5 problemas. Se deben entregar mínimo 4 de los 5 problemas.
	Tomar en cuenta las siguientes pruebas.
	Consideraciones generales</p>
	<ul>
		<li>- Crear un repositorio en Github / Gitlab o Bitbucket. Debe ser un repositorio privado.</li>
		<li>- El código debe poder ser ejecutado con versiones vanilla del lenguaje a utilizar.</li>
		<li>- Seguir estándares de código como si se fuera entregar a un cliente.</li>
		<li>- Simplicidad en la lógica utilizada.</li>
	</ul>
	
	<p>La implementación puede ser tan sencilla o complicada como desees, realiza la tarea de un
	modo que expongas de la mejor manera tus conocimientos.</p>
	
	<strong>1. Fetch & order</strong>
	<p>Usando https://randomuser.me/ como fuente de data, crear una función que retorne un arreglo:</p>
	<ul>
		<li>- Debe retornar 10 personas.</li>	
		<li>- Las personas deberán estar ordenadas por nombre.d</li>	
	</ul>
	<strong>2. Fetch & find</strong>
	<p>Usando https://randomuser.me/ como fuente de data, crear function, que acepte como>
	argumento una edad y retorne la data de la persona:</p>
	<ul>
		<li>- Retornará 1 sola persona.</li>	
		<li>- La persona a retornar será mayor de la edad especificada como argumento.</li>	
	</ul>
	
	<strong>3. Fetch & count</strong>
	<p>Usando https://randomuser.me/ como fuente de data, crear function, que retornará un
	char/string:</p>
	<ul>
		<li>- Deberá obtener 5 personas.</li>
		<li>- En base a los nombres deberá calcular cual es la letra más utilizada.</li>
	</ul>
	<strong>4. Fastest ship</strong>
	<p>Usando https://swapi.co/ como fuente de data, crear function, que retornará un string:</p>
	<ul>
		<li>- Aceptara un argumento tipo entero, que indicará la cantidad de pasajeros requerida.</li>
		<li>- Debera calcular sobre todas los starships</li>
		<li>- Retornara el nombre la nave que coincida con los siguientes parámetros:</li>
		<li>- Tiene la capacidad para transportar los pasajeros indicados</li>
		<li>- Puede viajar por al menos 1 semana.</li>
		<li>- Fue parte de la trilogía original (4, 5, 6)</li>
		<li>- Si más de una nave coincide con dichos parámetros, debera retornar la mas rapida.</li>
	</ul>
	<strong>5. Planet by terrain</strong>
	<p>Usando https://swapi.co/ como fuente de data, crear function, que retornará un string:</p>
	<ul>
		<li>- Aceptara un argumento tipo string, que indicará el tipo de terreno.</li>
		<li>- Retornara el nombre del planeta que coincida con los siguientes parámetros:</li>
		<li>- Coincide el tipo de terreno especificado como parámetro.</li>
		<li>- Si más de un planeta coincide con dichos parámetros, deberá retornar el que posea más población.</li>
	</ul>
	
	<blockquote class="blockquote text-right">
	Documento desarrollado por Rootstack S.A.
	0Prueba
	Versión: 2.0
	</blockquote>
</div>



