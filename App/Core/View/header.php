<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo URL."/css/style.css";?>">
</head>
<body>
<div id="page" class="container">

	<header>
		<blockquote class="blockquote text-right">
  			<p class="mb-0">Cesar Landazabal.</p>
  			<footer class="blockquote-footer">Prueba técnica <cite title="Source Title">rootstack</cite></footer>
		</blockquote>
	</header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<ul class="navbar-nav">
			<li class="nav-item active"><a href="/" class="nav-link" title="Inicio">Inicio</a></li>
			<li class="nav-item"><a href="?page=orderPage" class="nav-link" title="Inicio">Ejercicio1</a></li>
			<li class="nav-item"><a href="?page=findPage" class="nav-link" title="Inicio">Ejercicio2</a></li>
			<li class="nav-item"><a href="?page=countPage" class="nav-link" title="Inicio">Ejercicio3</a></li>
			<li class="nav-item"><a href="?page=shipPage" class="nav-link" title="Inicio">Ejercicio4</a></li>
			<li class="nav-item"><a href="?page=planet" class="nav-link" title="Inicio">Ejercicio5</a></li>
		</ul>
	</nav>
	
