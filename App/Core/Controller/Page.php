<?php

namespace Core\Controller;

class Page{
	
	/**
	 * Contiene el archivo template, php, tpl, que contiene la vista
	 * en este caso solo es compatible con php
	 * @var boolean, String
	 */
	var $template = false;

	/**
	 * La data, que contiene las variables a pasar a la vista
	 * @var array
	 */
	var $data = array();

	/**
	 * Constructor
	 */
	public function __construct(){

	}

	/**
	 * Establece el template, el archivo a usar
	 * @param [String] $template Nombre del archivo a incluir como view
	 */
	protected function setTemplateFile( $template ){

		$this->template = $template;
	}

	/**
	 * Retorna el nombre del template file
	 */
	public function getTemplateFile(){
		
		return $this->template;
	}


}