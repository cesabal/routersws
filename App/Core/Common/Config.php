<?php

namespace Core\Common;

/**
 * La clase carga parametros de configuración y paramtetros get
 * es un singleton que puede ser recuperado desde cualquier php que lo requiera
 */
class Config{

    /**
     * Instancia de la lcase
     * @var Config
     */
	private static $instance;

    /**
     * Contiene la configuracion
     * @var [Object]
     */
	var $config;

    /**
     * Constructor
     * @param [Array] $conf La configuracion cargada desde el archivo de configuración
     */
    private function __construct( $conf = null )
    {
    	if( is_null( $conf ) == false )
    	{
			$this->setConfig( $conf );
    	}
    	if( empty( $_GET ) === false )
    	{
    		$get = (object)$_GET;
    		$this->addConfig( 'get', $get );
    	}
    }

    /**
     * Determina si exioste o no una instancia creada de la clase con el fin de retornar siempre la misma
     * @param  [Array] $conf La configuracion cargada desde el archivo de configuración
     * @return [Config]       Config object
     */
    public static function getInstance( $conf = null )
    {
        if (!self::$instance instanceof self) {
        	if( is_null( $conf ) == false )
        	{
            	self::$instance = new self( $conf );
        	}
        	else
        	{
        		self::$instance = new self();
        	}
        }

        return self::$instance;
    }

    /**
     * Setea la configuracion
     * @param [Array] $conf Array desde el archivo de configuracion
     */
	private function setConfig( $conf ){

		$this->config = (object)$conf;
	}

    /**
     * Retorna la configuracion
     * @return [Object] la configuracion
     */
	public function getConfig(){

		return $this->config;
	}

    /**
     * Agrega un parametro a la configuracion
     * @param [String] $name  nombre del parametro
     * @param [] $value Valor, puede ser cualquier tipo de dato
     */
	public function addConfig( $name, $value )
	{
	
		$this->config->$name = $value;
	}

    /**
     * Retorna un valor de configuracion por su nombre
     * @param  [String] $key nombre del paramtro a retornar
     * @return El valor de la configuracion o falso si no existe
     */
	public function getConfigKey( $key ){

		$ret = false;

		if( array_key_exists( $key, $this->config ) ){

			$ret = $this->config->$key;
		}

		return $ret;

	}
	
}