<?php

/**
 * Incluye automáticamente el archivo de una una clase al ser instanciada
 * 
 * @param String $class namesapce de la clase a cargar
 */
spl_autoload_register(function( $class ){

    $classfile = str_replace("\\", "/", $class ) . '.php';

    if(FALSE === stream_resolve_include_path(  APP_PATH . '/' . $classfile) )
    {
    	return;
    }

    require_once( APP_PATH . '/' . $classfile );

});
