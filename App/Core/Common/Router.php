<?php

namespace Core\Common;

use Core\Common\Config;

/**
 * Se encarga de encontrar y enrutar los archivos y metodos
 */
class Router{
	
	/**
	 * [$routers description]
	 * @var array
	 */
	var $routers = array();
	var $config;

	/**
	 * [__construct description]
	 */
	public function __construct(){

		$this->config = Config::getInstance();
		$this->basePath = $this->config->getConfigKey('routerVar');
		require_once( APP_PATH .'/' . $this->basePath . '/' . 'Router.php' );

	}

	/**
	 * [add description]
	 * @param [type] $router [description]
	 */
	public function add( $router ){

		array_push($this->routers, $router);

	}

	// Resuelve el archivo controlador que debe levantar de acuerdo
	// al router definido en el path
	// @see ~conf[routerVar] // Directoriop de controllers
	public function resolve()
	{
		$base = strtolower($this->basePath);
		$page = null;
		$action = null;
		$args = array();

		if( $this->config->config->get )
		{
			$this->parserPageData( $this->config->config->get->$base, $page, $action, $args );
		}
		else
		{
			$this->parserPageData( $this->config->config->defaultPage, $page, $action, $args );
		}
		
		// Se obtiene el archivo correspondiente
		require_once( APP_PATH . "/" . $this->basePath . "/" . "$page.php" );

		$page = new $page;
		
		if( $action )
		{
			$page->$action();
		}
		else
		{
			$page->index();
		}

		return $page;

	}

	/**
	 * Retorna la data parseada de la pagina
	 * @param  [String] $pageData La ruta completa en la rl
	 * @param  [String] &$page    la pagina 
	 * @param  [String] &$action  La accion o metodo
	 * @param  [Array] &$args    Los argumentos
	 */
	private function parserPageData( $pageData, &$page, &$action, &$args ){

		$parts = explode( "/", $pageData );

		if( count( $parts ) == 1 ){
			
			$page = $parts[0];
			
			$action = false;
			$args = array();

		}elseif( count( $parts ) == 2 ){
			
			$page = $parts[0];
			$action = $parts[1];
			
			$args = array();

		}elseif( count( $parts ) > 2 ){
			
			$page = $parts[0];
			$action = $parts[1];
			
			unset($parts[0]);
			unset($parts[1]);

			$args = $parts;
		}
		else{
			$page = $this->config->config->defaultPage;
			$action = false;
			$args = array();
		}

	}

	/**
	 * Retorna los routers
	 * @return [type] [description]
	 */
	public function getRouters(){

		return $this->routers;
		
	}

	public function setRouterVar( $var ){

		$this->routerVar = $var;
	}
}