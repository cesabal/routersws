<?php

namespace Core;

use Core\Common\Config;
use Core\Common\Router;


class App{
	
	var $appConfig;
	var $appRouter;
	var $page;

	public function __construct( $conf ){

		$this->setConfig( $conf );
		$this->appRouter = $this->setRouter( $conf );
		$this->page = $this->appRouter->resolve();

	}

	// Ejecuta la App
	public function run(){

		$this->page;
		// $action = $this->pageInfo['action'];
		// $args = $this->pageInfo['args'];
		require( APP_PATH . "/Core/View/header.php" );
		if( $template = $this->page->getTemplateFile() )
		{
			extract( $this->page->data );
			require( APP_PATH . "/Core/View/" . $template );
		}
		else
		{
			require( APP_PATH . "/Core/View/default.php" );
		}
		require( APP_PATH . "/Core/View/footer.php" );
	}

	// Establece la configuración como una propiedad de App
	private function setConfig( $conf ){

		$this->appConfig = Config::getInstance( $conf );
	}

	private function setRouter(){

		return new Router();

	}
}